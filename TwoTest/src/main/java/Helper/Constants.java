package Helper;

public class Constants {
    final static public String BASE_URL= "https://dev-app.swiperapp.io" ;
    final static public String AUTH_MAPPING = "/auth/v1/email";
    final static public String DEFAULT_EMAIL = "Napoleon.Schaefer@yahoo.com";
    final static public String PASSWORD = "Password55!";
    final static public String CREATE_MAPPING = "/ts/v1/subscription";
    final static public String HASHED_EMAIL = "34d801abc2078d9d5de1198b6c1543ea7dd0b048bbb541ce503e3635b47e1ecc";
    final static public String APP_NAME = "SwiperUAT";
}
