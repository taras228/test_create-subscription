package Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthModel {
    public AuthModel(String email, String password){
        this.email = email;
        this.password = password;
    }
    private String email;
    private String password;
}
