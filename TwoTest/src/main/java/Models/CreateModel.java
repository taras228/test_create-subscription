package Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateModel {
//    public CreateModel(String hashedEmail, String appName, String sessionId) {
//        this.hashedEmail = hashedEmail;
//        this.appName = appName;
//        this.sessionId = sessionId;
//    }

    private String hashedEmail;
    private String appName;
    private String sessionId;
}
