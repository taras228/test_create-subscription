import Helper.Constants;
import Models.AuthModel;
import Models.CreateModel;
import StepDefinitions.StepsHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class CreateSubSteps {
    private StepsHelper helper = new StepsHelper();
    private Response response;
    private String sessionId;
    CreateModel models = new CreateModel();

    @Test
    @Given("The user is authorized in the swiper")
    public void theUserIsAuthorizedInTheSwiper() {
        AuthModel model = new AuthModel(Constants.DEFAULT_EMAIL, Constants.PASSWORD);
        response = helper.sendAuthRequest(model);
        Assertions.assertEquals(200, response.statusCode());
    }

    @When("I get sessionId from body")
    public void iGetSessionIdFromBody() {
        //sessionId = response.jsonPath().get("sessionId").toString();
        models.setSessionId(response.jsonPath().get("sessionId").toString());
    }

    @And("I set hashed email")
    public void iSetHashedEmail() {
        models.setHashedEmail(Constants.HASHED_EMAIL);
    }

    @And("I set app name")
    public void iSetAppName() {
        models.setAppName(Constants.APP_NAME);
    }

    @And("I send request POST method - v1\\subscription")
    public void iSendRequestPOSTMethodVSubscription() {
        response = helper.sendRequestToSubscribe(models);
    }

    @Then("Check status code {string} in response")
    public void checkStatusCodeInResponse(String statusCode) {
        Assertions.assertEquals(Integer.parseInt(statusCode), response.statusCode());
    }
}
