package StepDefinitions;

import Helper.Constants;
import Models.AuthModel;
import Models.CreateModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;

import static io.restassured.RestAssured.given;

public class StepsHelper {
    private final ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    public Response sendAuthRequest(AuthModel model) {
        Response response = given()
                .baseUri(Constants.BASE_URL)
                .contentType(ContentType.JSON)
                .body(mapper.writeValueAsString(model))
                .when()
                .post(Constants.AUTH_MAPPING);
        return response;
    }

    @SneakyThrows
    public Response sendRequestToSubscribe(CreateModel models) {
        Response response = given()
                .baseUri(Constants.BASE_URL)
                .contentType(ContentType.JSON)
                .body(mapper.writeValueAsString(models))
                .when()
                .post(Constants.CREATE_MAPPING);
        return response;
        }
}