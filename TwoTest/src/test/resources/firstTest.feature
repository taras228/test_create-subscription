Feature: Create subscription

  Scenario: Client subscription to push notifications
    Given The user is authorized in the swiper
    When I get sessionId from body
    And I set hashed email
    And I set app name
    And I send request POST method - v1\subscription
    Then Check status code "201" in response
